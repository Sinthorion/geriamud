require 'util/crypt_utils'
require 'util/storage_utils'
require 'util/persistent'

class PasswordException < Exception
end

class PasswordMismatchException < PasswordException
  def initialize
    super "The password does not match!"
  end
end

class PasswordMissingException < PasswordException
  def initialize
    super "Password required."
  end
end

class PasswordLengthException < PasswordException
  def initialize(min)
    super "Password too short. Minimum length is #{min} chars."
  end
end


# @!attribute name r [String]
# @!attribute name rw [String]
class User < Persistent

  attr :name, public: true, write: false
  attr :password, write: false
  attr :character

  UserData = Struct.new(:name, :password, :character, keyword_init: true)

  def initialize(name)
    super(name: name)
  end

  def id
    self.name
  end

  def entity
    'user'
  end

  def new?
    self.password.nil?
  end

  def login(password)
    unless CryptUtils.password_verify password, self.password
      raise PasswordMismatchException.new
    end
    @logged_in = true
  end

  def logged_in?
    @logged_in
  end

  # @param [String] password
  def set_password(password)
    raise PasswordException.new 'Already set' unless self.password.nil?
    raise PasswordLengthException.new 8 if password.length < 8
    @data[:password] = CryptUtils.password_hash password
  end
end

