require 'digest'
require 'yaml'
require 'fileutils'

module StorageUtils

  private

  STORAGE_ROOT = File.join($PROJECT_ROOT, 'db')
  SHA1 = Digest :SHA1

  def self.serialize(data)
    YAML.dump data
  end

  def self.deserialize(data)
    YAML::load data
  end

  public

  def self.locate(entity, uid)
    file = SHA1.hexdigest uid
    file += '.yaml'
    return File.join STORAGE_ROOT, entity, file
  end


  # @param [String] entity
  # @param [String] uid
  # @return [Object]
  def self.load(entity, uid)
    path = locate entity, uid.to_s
    return nil unless File.exist? path

    data = nil
    File.open path, 'r' do |file|
      data = deserialize file
    end
    return data
  end

  # @param [String] entity
  # @param [String] uid
  # @param [Object] data
  def self.save(entity, uid, data)
    path = locate entity, uid.to_s
    unless File.exist? path
      FileUtils::makedirs File.dirname path
    end

    File.open path, 'w' do |file|
      file.puts serialize data
    end
  end
end