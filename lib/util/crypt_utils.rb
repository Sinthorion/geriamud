require 'digest'
require 'securerandom'

module CryptUtils

  private

  SALT_LENGTH = 32
  HASH_ALGO = Digest :SHA256
  VERSION = 'sha256'
  SEPARATOR = ':'

  public

  def self.password_hash(password, salt = nil)
    salt = SecureRandom.hex SALT_LENGTH if salt.nil?
    hash = HASH_ALGO.hexdigest salt + password
    return [VERSION, salt, hash].join SEPARATOR
  end

  def self.password_verify(password, hash)
    version, salt, hash = hash.split ':'
    raise 'Unknown hash version' unless version == VERSION
    rehash = HASH_ALGO.hexdigest salt + password
    hash == rehash
  end
end