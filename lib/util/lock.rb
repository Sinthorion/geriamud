class Lock
  def initialize(inner)
    @inner = inner
    @mutex = Mutex.new
  end

  def get(&block)
    @mutex.synchronize do
      block.yield @inner
    end
  end
end