require 'util/storage_utils'
require 'digest'

class Persistent

  def Persistent.inherited(subclass)
    subclass.class_eval <<-RUBY
      @@fields = {}
      def self.fields
        @@fields
      end
      @@entity = subclass.to_s.downcase
      def self.entity
        @@entity
      end
    RUBY

  end

  def self.attr(name, default: nil, public: false, write: true, public_write: public && write)
    name = name.to_sym

    self.class_eval "@@fields[name] = default"

    self.define_method name do
      @data[name]
    end

    if public
      self.class_eval 'public name'
    else
      self.class_eval 'protected name'
    end

    setter = name.to_s + '='
    if write
      self.define_method setter do |val|
        @data[name] = val
      end

      if public_write
        self.class_eval 'public setter'
      else
        self.class_eval 'protected setter'
      end
    end

  end

  # @param [Object] id
  def initialize(id = nil, **kwargs)
    @data = {}

    self.class.fields.each do |key, val|
      sym = key.to_sym
      @data[sym] = val || kwargs[key]
    end

    if id.nil?
      id = kwargs.values.first
    end

    @id = id

    loaded = self.class.load(id)
    loaded.each do |key, val|
      @data[key.to_sym] = val unless val.nil?
    end unless loaded.nil?

    ObjectSpace.define_finalizer self, self.class.finalize(id, @data)
  end

  def ==(other)
    self.class == other.class && self.id == other.id
  end

  def eql?(other)
    self.class == other.class && self.id == other.id
  end

  def hash
    return self.id.hash
  end

  def [](key)
    @data[key.to_sym]
  end

  def []=(key, val)
    @data[key.to_sym] = val
    @dirty = true
  end

  def to_s
    "#{self.entity}(#{@data})"
  end

  def inspect
    self.to_s
  end

  def to_a
    @data.to_a
  end

  def to_h
    @data.clone
  end

  def values
    @data.values
  end

  def size
    @data.size
  end

  def length
    @data.length
  end

  def each(&block)
    @data.each &block
  end

  def each_pair(&block)
    @data.each &block
  end

  # @param [Object] id
  # @return [Hash]
  def self.load(id)
    StorageUtils.load entity, id.to_s
  end

  def save
    self.class.save(@id, @data)
  end

  def self.save(id, data)
    # data.each do |key, val|
    #   if val.is_a? Persistent
    #     val.save
    #     data[key] = val.id
    #   end
    # end
    StorageUtils.save entity, id, data
  end

  def self.finalize(id, data)
    proc { Persistent.save id, data }
  end
end
