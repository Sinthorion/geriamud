require 'game/user'

class Connection

  # @param server [MudServer]
  # @param client [TCPSocket]
  def initialize(server, client)
    # @type [MudServer]
    @server = server
    # @type [TCPSocket]
    @client = client

    @alive = true
  end

  def alive?
    @alive
  end

  def to_s
    _, port, _, ip = @client.peeraddr
    "#{ip}:#{port}"
  end

  def login?
    @client.print 'Please enter your username: '
    name = @client.gets.chomp
    @user = User.new name

    if @user.new?
      @client.print 'You seem to be new here. Please enter a password for your account: '
      begin
        password = @client.gets.chomp
        @user.set_password password
      rescue PasswordException => e
        @client.puts e
        @client.print 'Please try again: '
        retry
      end
      @user.save
      @client.puts 'Password set! You can use your username password combination to log in again in the future.'
    else
      @client.print 'Please enter your password: '
      fails = 0
      begin
        password = @client.gets.chomp
        @user.login password
      rescue PasswordException => e
        sleep 1 # slow down brute force
        @client.puts e
        fails += 1
        if fails > 3
          @client.puts 'Too many wrong attempts. Closing connection...'
          return false
        else
          @client.print "Password does not match.\nPlease try again: "
          retry
        end
      end
    end

    true
  end

  def close
    if self.alive?
      puts "disconnecting from #{self} (#{@user.name})"
      @client.close
      #@client = nil
      #@user = nil
      @alive = false
      @server.disconnect self
    end
  end

  def start
    @client.puts 'Welcome to GeriaMUD, being developed by Sinthorion!'

    self.login? or self.close

    @client.puts 'Thanks for coming by. As there\'s nothing to see here yet, we\'ll log you off again.', 'Bye!'

    self.close
  end
end