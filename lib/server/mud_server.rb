require 'socket'
require 'set'
require 'util/lock'
require 'server/connection'

class MudServer

  MAX_CONNECTIONS = 100

  def initialize(port)
    @connections = Lock.new Set.new

    @root_thread = nil
    # @type [TCPServer]
    @server = TCPServer.new port
  end

  def alive?
    @root_thread.alive?
  end

  def start
    @root_thread = Thread.start do
      loop do
        Thread.new @server.accept do |client|
          puts "incoming connection"

          conn = nil
          @connections.get do |connections|
            if connections.size >= MAX_CONNECTIONS
              client.puts 'Sorry, server has reached maximum amount of concurrent connections.'
              client.close
            elsif connections.include? client
              client.puts 'This client is already connected. Multiple concurrent connections from the same client are not allowed.'
              client.close
            else
              conn = Connection.new self, client
              connections.add conn
            end
          end
          conn.start unless conn.nil?
        end
      end
    end
    _, port, _, _ = @server.addr
    puts "Server is now listening on port #{port}"
    return @root_thread
  end

  def close
    @root_thread.kill if @root_thread and @root_thread.alive?
    @connections.get do |pool|
      pool.each do |conn|
        conn.close
      end
    end
    @server.close
  end

  def disconnect(connection)
    raise 'Connection still alive!' if connection.alive?
    @connections.get do |pool|
      pool.delete connection
    end
  end

end